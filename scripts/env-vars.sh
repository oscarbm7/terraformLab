
## Copyright (c) 2018, 2019, Oracle and/or its affiliates. All rights reserved
## Licensed under the Universal Permissive License v 1.0 as shown at http://oss.oracle.com/licenses/upl.
### Provider credentials
export TF_VAR_tenancy_ocid="ocid1.tenancy.oc1..aaaaaaaarz3z5ljm4dl2mnvjlaexdg3y7xvqqrvudu7osptf7zwaikkqqf6q"
export TF_VAR_user_ocid="ocid1.user.oc1..aaaaaaaa3vjtfwz4fo5vqjxszw64sitqsyjymbr2ilxc6ke36e5keghfdy6a"
export TF_VAR_fingerprint="4c:04:ee:bc:76:85:c0:08:11:5c:13:f7:6f:60:72:d9"
export TF_VAR_private_key_path="keys/private_oci_api_key.pem"
export TF_VAR_ssh_public_key=$(cat keys/public_key.pub)
### Instance credentials
export TF_VAR_ssh_public_key_path="keys/public_key.pub"
export TF_VAR_ssh_private_key_path="keys/private_key.ppk"

### Region
export TF_VAR_region="us-ashburn-1"

### AD
# Possible values are 1, 2, 3
export TF_VAR_AD="1"

### Compartment
export TF_VAR_compartment_ocid="ocid1.compartment.oc1..aaaaaaaa4yqtq4rh4tx3leoehegzeara6xyzvn4ii3bapacizevmfjmhonsa"

export TF_VAR_InstanceOSVersion=7.7

