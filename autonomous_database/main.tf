
resource "random_string" "autonomous_database_admin_password" {
  length      = 16
  min_numeric = 1
  min_lower   = 1
  min_upper   = 1
  min_special = 1
}


resource "oci_database_autonomous_database" "autonomous_database" {
  #Required
  admin_password           = random_string.autonomous_database_admin_password.result
  compartment_id           = var.compartment_ocid
  cpu_core_count           = "1"
  data_storage_size_in_tbs = "1"


  #Optional
  db_workload                                    = var.autonomous_database_db_workload
  display_name                                   = var.autonomous_database_name
  freeform_tags                                  = var.autonomous_database_freeform_tags
  is_auto_scaling_enabled                        = "true"
  license_model                                  = var.autonomous_database_license_model
  is_preview_version_with_service_terms_accepted = "false"

  #multiples environments
  #count = length(var.data_base_list)
  count = length(var.data_base_list_2)
  db_name = "${var.data_base_list[count.index]}${var.autonomous_database_name}"
}

output "showDataBase" {
  value = oci_database_autonomous_database.autonomous_database
}

