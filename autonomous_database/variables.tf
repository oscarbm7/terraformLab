###########autonomous variables#######################
variable "autonomous_database_db_workload" {
  default = "OLTP"
}

variable "autonomous_data_warehouse_db_workload" {
  default = "DW"
}

variable "autonomous_database_defined_tags_value" {
  default = "value"
}

variable "autonomous_database_freeform_tags" {
  default = {
    "Department" = "Finance"
  }
}

variable "autonomous_database_license_model" {
  default = "LICENSE_INCLUDED"
  ##default = "BRING_YOUR_OWN_LICENSE"
}

variable "autonomous_database_is_dedicated" {
  default = false
}

variable "autonomous_database_name" {
  default = "AtpDB"
}

variable "compartment_ocid" {
}

variable data_base_list{
  type = "list"
  default = ["Dev","Qa","Prod"]
}

variable data_base_list_2{
  type = "list"
  default = ["Dev"]
}