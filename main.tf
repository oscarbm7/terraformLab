# Configure the Oracle Cloud Infrastructure provider with an API Key
provider "oci" {
  tenancy_ocid     = var.tenancy_ocid
  user_ocid        = var.user_ocid
  fingerprint      = var.fingerprint
  private_key_path = var.private_key_path
  region           = var.region
  version          = ">= 3.43"
}

# Gets a list of Availability Domains
/*
data "oci_identity_availability_domains" "ads" {
  compartment_id = "${var.tenancy_ocid}"
}
*/
module "oci_database_autonomous_database"{
  source = "./autonomous_database"
  compartment_ocid = var.compartment_ocid
}






